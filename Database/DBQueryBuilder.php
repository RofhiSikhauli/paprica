<?php


/**
 * File name: DBQueryBuilder.php
 * Description: This file holds DBQueryBuilder class methods that generate database queries using method chaining,
 * Purpose: This file made for Paprica
 * Date: 15 October 2015
 * Author: Rofhiwa Sikhauli
 * Version v1.0.2
 */

namespace Paprica\Database;

use \PDO;

//class DBQueryBuilder is extending DBConfig class
final class DBQueryBuilder extends DBConfig{

	private $prepare, $exec = null;
	private $query = null;

	//include traits class
	use SelectQueryBuilder;
	use InsertQueryBuilder;    
	use UpdateQueryBuilder;


	public function __construct($query){

		$this->query = $query;
		parent::__construct();

	}


	/**
	  * Query where clause
	  *	@param query where clause condition 
	  * @return this object
	  *
	  */
	public function where($condition = NULL){

		$this->query .= " WHERE ". $condition;

		return $this;

	}

	/**
	  * Query private execute method
	  *	@param array of params 
	  * @return void
	  *
	  */
	private function execute(array $params = NULL){

		$this->prepare = $this->link->prepare($this->query);

		$this->exec = $this->prepare->execute($params);

	}


	/**
	  * Query fetch one row
	  *	@param array of binded params
	  * @return query results
	  *
	  */
	public function fetch(array $params = NULL){

		$this->execute($params);

		return ($this->exec) ? $this->prepare->fetch(PDO::FETCH_ASSOC) : false;

	}


	/**
	  * Query fetch all rows
	  *	@param array of binded params 
	  * @return query results
	  *
	  */
	public function fetch_all(array $params = NULL){

		$this->execute($params);

		return ($this->exec) ? $this->prepare->fetchAll(PDO::FETCH_ASSOC) : false;

	}


	/**
	  * Query count table rows
	  *	@param array of binded params 
	  * @return int number of rows
	  *
	  */
	public function row_count(array $params = NULL){

		$this->execute($params);

		return ($this->exec) ? $this->prepare->rowCount() : 0;

	}

	/**
	  * get last inserted row Auto increment id
	  *	@param none
	  * @return int lastInsertId
	  *
	  */
	public function getLastId(){

		return $this->link->lastInsertId();

	}

	/**
	  * Query run method
	  *	@param array of binded params 
	  * @return 
	  *
	  */
	public function run(array $params = NULL){

		$this->execute($params);

		return ($this->exec) ? 1 : $this->link->errorInfo();
	}


	/**
	  * PHP Magic method __toString()
	  *	@param none
	  * @return query
	  *
	  */
	public function __toString(){

		return $this->query;

	}



}








