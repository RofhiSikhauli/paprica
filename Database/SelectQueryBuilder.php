<?php

/**
 * File name: SelectQueryBuilder.php
 * Description: This file holds SelectQueryBuilder trait class methods that generate database queries using method chaining,
 * Purpose: This file made for Paprica
 * Date: 15 October 2015
 * Author: Rofhiwa Sikhauli
 * Version v1.0.2
 *
 */

namespace Paprica\Database;

trait SelectQueryBuilder{

	/**
	  * Query from method
	  *	@param table name 
	  * @return SelectQueryBuilder object
	  *
	  */
	public function from($table_name = NULL){

		if($table_name === NULL){
			throw new CustomException("Null table name");
		}

		$this->query .= " FROM ". $table_name;

		return $this;

	}

	/**
	  * Query group_by method
	  *	@param group by clause 
	  * @return SelectQueryBuilder object
	  *
	  */
	public function group_by($group_by = NULL){

		$this->query .= " GROUP BY ". $group_by;

		return $this;

	}


	/**
	  * Query order_by method
	  *	@param order by clause 
	  * @return SelectQueryBuilder object
	  *
	  */
	public function order_by($order_by = NULL){

		$this->query .= " ORDER BY ". $order_by;

		return $this;

	}


	/**
	  * Query from method
	  *	@param inner join clause 
	  * @return SelectQueryBuilder object
	  *
	  */
	public function inner_join($inner_join = NULL){

		$this->query .= " INNER JOIN ". $inner_join;

		return $this;

	}

	/**
	  * Query left_join method
	  *	@param left join clause  
	  * @return SelectQueryBuilder object
	  *
	  */
	public function left_join($left_join = NULL){

		$this->query .= " LEFT JOIN ". $left_join;

		return $this;

	}

	/**
	  * Query right_join method
	  *	@param right join clause
	  * @return SelectQueryBuilder object
	  *
	  */
	public function right_join($right_join = NULL){

		$this->query .= " RIGHT JOIN ". $right_join;

		return $this;

	}

	/**
	  * Query outer_join method
	  *	@param outer join clause 
	  * @return SelectQueryBuilder object
	  *
	  */
	public function outer_join($outer_join = NULL){

		$this->query .= " OUTER JOIN ". $outer_join;

		return $this;

	}


	/**
	  * Query join_on method
	  *	@param where to join table clause 
	  * @return SelectQueryBuilder object
	  *
	  */
	public function join_on($join_on = NULL){

		$this->query .= " ON ". $join_on;

		return $this;

	}

	/**
	  * Query limit method
	  *	@param table query select limit 
	  * @return SelectQueryBuilder object
	  *
	  */
	public function limit($start, $finish){

		$this->query .= " LIMIT ". $start . ", " . $finish;

		return $this;

	}


}
