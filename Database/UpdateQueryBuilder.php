<?php



/**
 * File name: UpdateQueryBuilder.php
 * Description: This file holds UpdateQueryBuilder class methods that generate database update queries using method chaining,
 * Purpose: This file made for Paprica
 * Date: 15 October 2015
 * Author: Rofhiwa Sikhauli
 * Version v1.0.2
 *
 */

namespace Paprica\Database;

trait UpdateQueryBuilder {


	/**
	  * Query update set method
	  *	@param array of columns to update
	  * @return UpdateQueryBuilder object
	  *
	  */
	public function set($cols = NULL){

		$arr = "";

		if(is_array($cols)){

			foreach ($cols as $key => $value) {

				if($arr === ""){
					$arr = $key . " = \"" . $value . "\""; 
				}else{
					$arr .= ", " . $key . " = \"" . $value . "\"";
				}
			}

		} else {

			$arr = $cols;
		}

		$this->query .= " SET " . $arr;

		return $this;

	}



}
