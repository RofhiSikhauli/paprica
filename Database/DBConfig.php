<?php


/**
 * File name: DBConfig.php
 * Description: This file holds DBConfig class, and two methods construct and connect to connect to database
 * Purpose: This file made for Paprica
 * Date: 15 October 2015
 * Author: Rofhiwa Sikhauli
 * Version v1.0.2
 *
 */

namespace Paprica\Database;

use \PDO;

class DBConfig{

	protected $link;
	const DB_TYPE = 'mysql';
	const DB_HOST = 'localhost';
	const DB_NAME = 'TSVAS';
	const DB_USER = 'rofhiwa';
	const DB_PASS = '5g062skSHfvSLJB';

	public function __construct(){

		try{
			$this->link = new PDO(self::DB_TYPE . ':host=' . self::DB_HOST . ';dbname=' . self::DB_NAME, self::DB_USER, self::DB_PASS); //Connect to the database
		}
		catch(PDOException $e){
			exit("DD Connect error: " . $e->getMessage());
		}
	}	

}