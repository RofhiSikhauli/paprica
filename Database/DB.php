<?php

/**
 * File name: DB.php
 * Description: This file holds static methods for select, instert, update and delete to create queries
 * Purpose: This file made for Paprica
 * Date: 15 October 2015
 * Author: Rofhiwa Sikhauli
 * Version v1.0.2
 */

namespace Paprica\Database;

use Paprica\Database\DBQueryBuilder;

class DB{

	private static $query  = null;

	/**
	  * Query select method
	  *	@param array of columns to select
	  * @return DBQueryBuilder object
	  *
	  */
	public static function select($args = NULL){

		if(is_array($args)){
			$cols = ($args === NULL) ? "*" : implode(", ", $args);
		}
		else{
			$cols = ($args === NULL) ? "*" : $args;
		}
		
		self::$query = "SELECT " . $cols;

		return new DBQueryBuilder(self::$query);

	}


	/**
	  * Query insert method
	  *	@param table name
	  * @return DBQueryBuilder object
	  *
	  */
	public static function insert($table_name = NULL){

		if($table_name === NULL){
			throw new CustomException("Null table name");
		}

		self::$query = "INSERT INTO " . $table_name;

		return  new DBQueryBuilder(self::$query);

	}



	/**
	  * Query update method
	  *	@param table name
	  * @return DBQueryBuilder object
	  *
	  */
	public static function update($table_name = NULL){

		if($table_name === NULL){
			throw new CustomException("Null table name");
		}

		self::$query = "UPDATE " . $table_name;

		return new DBQueryBuilder(self::$query);

	}


	/**
	  * Query delete method
	  *	@param table name
	  * @return DBQueryBuilder object
	  *
	  */

	public static function delete($table_name = NULL){

		if($table_name === NULL){
			throw new CustomException("Null table name");
		}

		self::$query = "DELETE FROM " . $table_name;

		return new DBQueryBuilder(self::$query);

	}


	/**
	  * PHP Magic method __toString()
	  * @return Query string
	  *
	  */
	public function __toString(){

		return self::$query;

	}
	 


}



