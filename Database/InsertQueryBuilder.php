<?php


/**
 * File name: InsertQueryBuilder.php
 * Description: This file holds InsertQueryBuilder class methods that generate database queries using method chaining,
 * Purpose: This file made for Paprica
 * Date: 15 October 2015
 * Author: Rofhiwa Sikhauli
 * Version v1.0.2
 *
 */

namespace Paprica\Database;

trait InsertQueryBuilder{


	/**
	  * Query insert columns
	  *	@param array of colums names
	  * @return InsertQueryBuilder object
	  *
	  */
	public function columns($cols = NULL){

		if($cols === NULL){
			throw new CustomException("Null table name");
		}

		$this->query .= " (" . implode(", ", $cols) . ") ";

		return $this;

	}

	/**
	  * Query insert columns values
	  *	@param array of colums values
	  * @return InsertQueryBuilder object
	  *
	  */
	public function values($vals = NULL){

		if($vals === NULL){
			throw new CustomException("Null table name");
		}

		$this->query .= " VALUES (" . implode(", ", $vals) . ") ";
		return $this;

	}


}
