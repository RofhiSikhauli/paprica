<?php

use Paprica\Database\DB;

class LogsModel{

	private static $tbl_subcriber 	= "subscriber";
	private static $tbl_port 	 	= "cellc_porting";
	private static $response 		= null;

	//PHP class magic method __construct
	public function __construct(){}

	//PHP class magic method __destruct
	public function __destruct(){

		self::$response = null; //Reset response value

	}

	/**
	  * Log port data
	  * @param array data
	  * @return database response
	  *
	  */
	protected static function log_port($table_data){

		$cols = array(
			"simserial", 
			"QAgentId", 
			"isSuccessful",
			"received_wsdl",
			"port_date",
			"port_number",
			"firstname", 
			"surname", 
			"idtype", 
			"idnumber", 
			"address", 
			"suburb", 
			"city", 
			"postalcode", 
			"AccessMech"
		);

		$cols_vals = array(
			":simserial", 
			":QAgentId", 
			":isSuccessful",
			":received_wsdl",
			":port_date",
			":port_number",
			":firstname", 
			":surname", 
			":idtype", 
			":idnumber", 
			":address", 
			":suburb", 
			":city", 
			":postalcode", 
			":AccessMech"
		);


		$table_data['port_date'] = date("Y-m-d H:i:s");

		self::$response = DB::insert(self::$tbl_port)->columns($cols)->values($cols_vals)->run($table_data);

		return self::$response;

	}


	/**
	  * Log rica data
	  * @param array data
	  * @return database response
	  *
	  */
	protected static function log_rica($table_data){

		$cols = array(
			"simserial", 
			"QAgentId", 
			"isSuccessful",
			"received_wsdl",
			"ricad_date",
			"firstname", 
			"surname", 
			"idtype", 
			"idnumber", 
			"streetname", 
			"suburb", 
			"city", 
			"postalcode", 
			"AccessMech"
		);

		$cols_vals = array(
			":simserial", 
			":QAgentId", 
			":isSuccessful",
			":received_wsdl",
			":ricad_date",
			":firstname", 
			":surname", 
			":idtype", 
			":idnumber", 
			":streetname", 
			":suburb", 
			":city", 
			":postalcode", 
			":AccessMech"
		);

		$table_data = self::change_key($table_data, "address", "streetname");

		unset($table_data['port_number']);

		$table_data['ricad_date'] = date("Y-m-d H:i:s");

		self::$response = DB::insert(self::$tbl_subcriber)->columns($cols)->values($cols_vals)->run($table_data);

		return self::$response;

	}

	/**
	  * Change array  key
	  * @param array data
	  * @param array old key
	  * @param array new key
	  * @return array
	  *
	  */
	private static function change_key($array, $old_key, $new_key) {

	    if(!array_key_exists($old_key, $array)){
	        return $array;
	    }

	    $keys = array_keys($array);
	    $keys[array_search($old_key, $keys)] = $new_key;

	    return array_combine($keys, $array);
	}




}