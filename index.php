<?php

header("Access-Control-Allow-Origin: http://10.0.0.0");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: X-Requested-With");
header("Cache-Control: no-store, no-cache, must-revalidate, private, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("X-Frame-Options: DENY");
header("Connection: close");
header("X-XSS-Protection: 1");
header("X-Content-Type-Options: nosniff");

date_default_timezone_set('Africa/Johannesburg'); //Set default timezone

use Paprica\Database\DB;
use Paprica\Models\LogsModel;

$url 		= null;
$method		= $_SERVER["REQUEST_METHOD"];
$response	= array();
$obj		= null;
$params 	= null;

if(isset($_GET['url'])){
	$url = explode("/", $_GET['url'])[0];
}

//Check if method assigned
if($url == null){
	exit("Invalid URL specified");
}

//Allow only POST request method
if($method != "POST" || $_SERVER["HTTPS"] !== "on"){
	exit("Invalid request");
}

//Auto load classes
function __autoload($class_name){

	$folders = array(
		"",
		"Database/", 
		"Controllers/",
		"Models/",
	);

	foreach ($folders as $folder) {

		$get_file = explode("\\", $class_name);

		$filename = $folder . end($get_file) . ".php";

		if(file_exists($filename)){
			include($filename);
		}
	}
}

$request_data = file_get_contents("php://input"); //Get posted data 

if ($request_data) {
	$params = json_decode($request_data);
} else {
	exit("Required parameters missing");
}

switch ($url) {

	case 'PerformPortAndRica':

		$access_mech	= validate("Access mechanism", $params->access_mech);
		$qagent_id		= validate("QAgent ID", $params->qagent_id);
		$serial_number	= validate("Serial number", $params->serial_number);		
		$port_number	= validate("Port number", $params->port_number);		
		$id_type_code	= validate("ID type", $params->id_type_code);		
		$id_number		= validate("ID number", $params->id_number);		
		$first_name		= validate("First name", $params->first_name);	
		$last_name		= validate("Last name", $params->last_name);	
		$address		= validate("Address", $params->address);	
		$suburb			= validate("Suburb", $params->suburb);
		$city			= validate("City", $params->city);
		$postal_code	= validate("Postal code", $params->postal_code);		
		
		$response = $url::perform_port_and_rica($access_mech, $qagent_id, $serial_number, $port_number, $id_type_code, $id_number, $first_name, $last_name, $address, $suburb, $city, $postal_code);

		break;

	case 'PerformRica':
		
		$access_mech	= validate("Access mechanism", $params->access_mech);
		$qagent_id		= validate("QAgent ID", $params->qagent_id);
		$serial_number	= validate("Serial number", $params->serial_number);			
		$id_type_code	= validate("ID type", $params->id_type_code);		
		$id_number		= validate("ID number", $params->id_number);		
		$first_name		= validate("First name", $params->first_name);	
		$last_name		= validate("Surname", $params->last_name);	
		$address		= validate("Address", $params->address);	
		$suburb			= validate("Suburb", $params->suburb);
		$city			= validate("City", $params->city);
		$postal_code	= validate("Postal code", $params->postal_code);	

		$response = $url::perform_rica($access_mech, $qagent_id, $serial_number, $id_type_code, $id_number, $first_name, $last_name, $address, $suburb, $city, $postal_code);

		break;

	case 'PerformPortRequest':

		$access_mech	= validate("Access mechanism", $params->access_mech);
		$qagent_id		= validate("QAgent ID", $params->qagent_id);
		$port_number	= validate("Port number", $params->port_number);		
		$serial_number	= validate("Serial number", $params->serial_number);

		$response = $url::port_request($access_mech, $qagent_id, $serial_number, $port_number);

		break;

	case 'QueryPort':
		
		$qagent_id		= validate("QAgent ID", $params->qagent_id);
		$port_number	= validate("Port number", $params->port_number);		
		$serial_number	= validate("Serial number", $params->serial_number);		

		$response = $url::query_port($port_number, $serial_number);

		break;

	case 'PerformPortAuthorisation':
		
		$qagent_id		= validate("QAgent ID", $params->qagent_id);
		$port_number	= validate("Port number", $params->port_number);		
		$serial_number	= validate("Serial number", $params->serial_number);		
		$auth_code		= validate("Authorisation code", $params->auth_code);		

		$response = $url::perform_port_authorisation($port_number, $serial_number, $auth_code);

		break;
	
	default:         
		$response['responseCode']  = 404;
        $response['responseMessage'] = "Unknown request";
		break;
}

function validate($param_name, $param_value) {

	if ($param_value == null) {
		exit($param_name . " missing");
	} else {
		return $param_value;
	}

}


echo json_encode($response);