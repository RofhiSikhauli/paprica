<?php


class PerformRica Extends Paprica{


	public static function perform_rica($access_mech, $qagent_id, $serial_number, $id_type_code, $id_number, $first_name, $last_name, $address, $suburb, $city, $postal_code){

		$wsdl = null;

		$data = array(
				"type"			=> "rica",
				"qagent_id" 	=> $qagent_id,
				"serial_number" => $serial_number,
				"id_type" 		=> $id_type_code,
				"id_number" 	=> $id_number,
				"first_name" 	=> $first_name,
				"last_name" 	=> $last_name,
				"streetname" 	=> $address,
				"suburb" 		=> $suburb,
				"city" 			=> $city,
				"postal_code" 	=> $postal_code,
				"access_mech"	=> $access_mech
			);

		switch ($id_type_code) {

			case '1':
				$wsdl = self::south_african_subscriber($serial_number, $id_number, $first_name, $last_name, $address, $suburb, $city, $postal_code);
				break;

			case '2':
				$wsdl = self::foreign_subscriber($serial_number, $id_number, $first_name, $last_name, $address, $suburb, $city, $postal_code);
				break;
			
			default:
				exit("An unknown error occurred");
				break;
		}

		return parent::soap_request($wsdl, $data);

	}

	private static function south_african_subscriber($serial_number, $id_number, $first_name, $last_name, $address, $suburb, $city, $postal_code){

		$soapData =  "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:pap='http://paprica.ws.soa.cellc.co.za/'>
					  <soapenv:Header/>
					  <soapenv:Body>
					      <pap:PerformRica>
					         <ricaInput>
					         	<process>
					               <ssUID>?</ssUID>
					               <asqUID>?</asqUID>
					               <sessionId>" . parent::$session_id . "</sessionId>
						           <username>?</username>
						           <password>?</password>
								   <asCode>PapricaService</asCode>
								   <aspCode>PerformRica</aspCode>
					               <consumerRef>?</consumerRef>
					               <data>?</data>
					               <endTime>?</endTime>
					               <entityCode>?</entityCode>
					               <note>?</note>
					               <request>?</request>
					               <response>?</response>
					               <severity>?</severity>
					               <startTime>?</startTime>
					               <providerId>?</providerId>
					               <state>?</state>
					            </process>
					            <pap:UsernamePasswordCredentials>
					           	   <username>" . parent::$username . "</username>
					           	   <password>" . parent::$password . "</password>
					            </pap:UsernamePasswordCredentials>
					            <simSerialNumber>" . $serial_number . "</simSerialNumber>
					            <pap:SouthAfricanSubscriber>
					               <address>" . $address . "</address>
					               <city>" . $city . "</city>
					               <name>" . $first_name . "</name>
					               <postalCode>" . $postal_code . "</postalCode>		             
					               <suburb>" . $suburb . "</suburb>
					               <surname>" . $last_name . "</surname>
					               <idNumber>" . $id_number . "</idNumber>
					            </pap:SouthAfricanSubscriber>
					         </ricaInput>
					      </pap:PerformRica>
					   </soapenv:Body>
					</soapenv:Envelope>";

		return $soapData;

	}

	private static function foreign_subscriber($serial_number, $passport_number, $first_name, $last_name, $address, $suburb, $city, $postal_code){

		$soapData = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:pap='http://paprica.ws.soa.cellc.co.za/'>
				   	 <soapenv:Header/>
				     <soapenv:Body>
				      <pap:PerformRica>
				         <!--Optional:-->
				         <ricaInput>
				         	<process>
				               <ssUID>?</ssUID>
				               <asqUID>?</asqUID>
				               <sessionId>" . parent::$session_id . "</sessionId>
					           <username>?</username>
					           <password>?</password>
							   <asCode>PapricaService</asCode>
							   <aspCode>PerformRica</aspCode>
				               <consumerRef>?</consumerRef>
				               <data>?</data>
				               <endTime>?</endTime>
				               <entityCode>?</entityCode>
				               <note>?</note>
				               <request>?</request>
				               <response>?</response>
				               <severity>?</severity>
				               <startTime>?</startTime>
				               <providerId>?</providerId>
				               <state>?</state>
				            </process>
				            <pap:UsernamePasswordCredentials>
				           	   <username>" . parent::$username . "</username>
				           	   <password>" . parent::$password . "</password>
				            </pap:UsernamePasswordCredentials>
				            <simSerialNumber>" . $serial_number . "</simSerialNumber>
				            <pap:ForeignSubscriber>
				               <name>" . $first_name . "</name>
				               <surname>" . $last_name . "</surname>
				               <address>" . $address . "</address>
				               <suburb>" . $suburb . "</suburb>
				               <city>" . $city . "</city>
				               <postalCode>" . $postal_code . "</postalCode>
				               <passportNumber>" . $passport_number . "</passportNumber>
				            </pap:ForeignSubscriber>
				         </ricaInput>
				      </pap:PerformRica>
				   </soapenv:Body>
				</soapenv:Envelope>";

		return $soapData;

	}


		 
}
