<?php


class PerformPortAuthorisation Extends Paprica{

	public static function perform_port_authorisation($port_number, $serial_number, $auth_code){

		$data = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:pap='http://paprica.ws.soa.cellc.co.za/'>
				   <soapenv:Header/>
				   <soapenv:Body>
				      <pap:PerformPortAuthorisation>
				         <portInput>
			            	<pap:UsernamePasswordCredentials>
				           	   <username>" . parent::$username . "</username>
				           	   <password>" . parent::$password . "</password>
				            </pap:UsernamePasswordCredentials>
				            <process>
				               <sessionId>" . parent::$session_id . "</sessionId>
				               <asCode>PapricaService</asCode>
				               <aspCode>PerformPortAuthorisation</aspCode>
				            </process>
				            <authorisationCode>" . $auth_code . "</authorisationCode>
				            <portNumber>" . $port_number . "</portNumber>
				            <simSerialNumber>" . $serial_number . "</simSerialNumber>
				         </portInput>
				      </pap:PerformPortAuthorisation>
				   </soapenv:Body>
				</soapenv:Envelope>";






		return parent::soap_request($data);

	}
		 
}
