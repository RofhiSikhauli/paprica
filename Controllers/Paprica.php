<?php

class Paprica extends LogsModel{

  private static $curl = null;

  /**
    * Set curl header
    * @param xml data
    * @return array
    *
    */
  private static function set_header($data){

      $header = array(
          "Content-Type: text/xml;charset=UTF-8",
          "Accept: gzip,deflate",
          "Cache-Control: no-cache",
          "Pragma: no-cache",
          "SOAPAction: \"\"",
          "Content-length: " . strlen($data),
      );

      return $header;
  }


  /**
    * get responseCode
    * @param xml response
    * @return json
    *
    */
  private static function getResponse($responseData, $userData){

      $response     = array();
      $dom          = new DOMDocument();

      $dom->loadXML($responseData);
      $responseCode     = $dom->getElementsByTagName('responseCode')->item(0)->nodeValue;
      $responseMessage  = $dom->getElementsByTagName('responseMessage')->item(0)->nodeValue;

      if($responseCode != "" || $responseMessage != ""){

          $response['responseCode']  = $responseCode;
          $response['responseMessage'] = $responseMessage;

          if($userData != null){

              $table_data  = array(
                  "simserial"     => (isset($userData['serial_number']) ? $userData['serial_number'] : NULL), 
                  "QAgentId"      => (isset($userData['qagent_id']) ? $userData['qagent_id'] : NULL), 
                  "isSuccessful"  => (($responseCode == 202) ? 1 : 0),
                  "received_wsdl" => $responseMessage,
                  "port_number"   => (isset($userData['port_number']) ? $userData['port_number'] : NULL),
                  "firstname"     => (isset($userData['first_name']) ? $userData['first_name'] : NULL), 
                  "surname"       => (isset($userData['last_name']) ? $userData['last_name'] : NULL), 
                  "idtype"        => (isset($userData['id_type']) ? $userData['id_type'] : NULL), 
                  "idnumber"      => (isset($userData['id_number']) ? $userData['id_number'] : NULL), 
                  "address"       => (isset($userData['address']) ? $userData['address'] : NULL),
                  "suburb"        => (isset($userData['suburb']) ? $userData['suburb'] : NULL), 
                  "city"          => (isset($userData['city']) ? $userData['city'] : NULL), 
                  "postalcode"    => (isset($userData['postal_code']) ? $userData['postal_code'] : NULL), 
                  "AccessMech"    => (isset($userData['access_mech']) ? $userData['access_mech'] : 0)
              );

              self::log_port_and_rica($table_data, $userData['type']);
          }

      } else {

          $response['responseCode']  = 500;
          $response['responseMessage'] = "An unknown error occurred";

      }

      return $response;
  }


    /**
    * Log porting and rica data
    * @param array data
    * @return null
    *
    */
    private static function log_port_and_rica($table_data, $type){

        switch ($type) {
          case 'all':

            if(parent::log_port($table_data) === 1){
                parent::log_rica($table_data);
            } 
            
            break;
          case 'port':
            parent::log_port($table_data);
            break;
          case 'rica':
            parent::log_rica($table_data);
            break;
          
          default:
            
            break;
        }
    }



  /**
    * Make curl request
    * @param xml data
    * @return array
    *
    */
  protected static function soap_request($wsdl, array $data = null){

      $header = self::set_header($wsdl);

      self::$curl = curl_init(self::$url);
      curl_setopt(self::$curl, CURLOPT_POST, 1);
      curl_setopt(self::$curl, CURLOPT_TIMEOUT, 10);
      curl_setopt(self::$curl, CURLOPT_VERBOSE, 1);
      curl_setopt(self::$curl, CURLOPT_POSTFIELDS, $wsdl);
      curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt(self::$curl, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt(self::$curl, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt(self::$curl, CURLOPT_HTTPHEADER, $header);
      $result = curl_exec(self::$curl);
      curl_close(self::$curl);

      return self::getResponse($result, $data);

  }


}