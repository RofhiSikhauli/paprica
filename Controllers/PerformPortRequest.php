<?php


class PerformPortRequest extends Paprica{

	public static function port_request($access_mech, $qagent_id, $serial_number, $port_number){

		$data = array(
				"type"			=> "port",
				"qagent_id" 	=> $qagent_id,
				"serial_number" => $serial_number,
				"port_number" 	=> $port_number,
				"access_mech"	=> $access_mech
			);

		$wsdl = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:pap='http://paprica.ws.soa.cellc.co.za/'>
			    <soapenv:Header/>
			    <soapenv:Body>
			      <pap:PerformPortRequest>
			         <portInput>
			            <process>
			               <ssUID>?</ssUID>
			               <asqUID>?</asqUID>	  
			               <sessionId>" . parent::$session_id . "</sessionId>
			           	   <username>?</username>
			           	   <password>?</password>
						   <asCode>PapricaService</asCode>
						   <aspCode>PerformPortRequest</aspCode>	  
			               <consumerRef>?</consumerRef>	  
			               <data>?</data>	  
			               <endTime>?</endTime>	  
			               <entityCode>?</entityCode>	  
			               <note>?</note>	  
			               <request>?</request>	  
			               <response>?</response>	  
			               <severity>?</severity>	  
			               <startTime>?</startTime>
			               <providerId>?</providerId>
			               <state>?</state>
			            </process>
			            <pap:UsernamePasswordCredentials>
			           	   <username>" . parent::$username . "</username>
			           	   <password>" . parent::$password . "</password>
			            </pap:UsernamePasswordCredentials>
			            <portNumber>" . $port_number . "</portNumber>
			            <simSerialNumber>" . $serial_number . "</simSerialNumber>
			         </portInput>
			       </pap:PerformPortRequest>
			    </soapenv:Body>
			</soapenv:Envelope>";

		return parent::soap_request($wsdl, $data);

	}
		 
}
