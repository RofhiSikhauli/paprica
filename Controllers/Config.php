<?php

class Config {
  public static $username    = "username"; 
  public static $password    = "password"; 
  public static $session_id  = "t6ytrjfgrhgrjbg8r7hgurhgrhgurufg";
  public static $url         = "https://0.0.0.0:443/PAPRICA/PapricaService?wsdl";
  public static $namespace	 = "http://namespace.co.za";
  public static $log_file	 = "/path/to/logFile.log";
}