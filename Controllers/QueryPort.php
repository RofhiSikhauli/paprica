<?php


class QueryPort Extends Paprica{

	public static function query_port($port_number, $serial_number){

		$data = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:pap='http://paprica.ws.soa.cellc.co.za/'>
				   <soapenv:Header/>
				   <soapenv:Body>
				      <pap:QueryPort>
				         <portInput>
				            <pap:UsernamePasswordCredentials>
				           	   <username>" . parent::$username . "</username>
				           	   <password>" . parent::$password . "</password>
				            </pap:UsernamePasswordCredentials>
				            <process>
				               <sessionId>" . parent::$session_id . "</sessionId>
				               <asCode>PapricaService</asCode>
				               <aspCode>QueryPort</aspCode>
				            </process>
				            <portNumber>" . $port_number . "</portNumber>
				            <simSerialNumber>" . $serial_number . "</simSerialNumber>
				         </portInput>
				      </pap:QueryPort>
				   </soapenv:Body>
				</soapenv:Envelope>";
			
		return parent::soap_request($data);

	}
		 
}
